USE [getPalace]
GO

CREATE PROCEDURE [dbo].[DeleteOwnerDetails]
      
AS

BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
       DELETE from OwnerDetails
	   DBCC CHECKIDENT ('OwnerDetails', RESEED , 0);

END
