USE [getPalace]
GO

CREATE TABLE [dbo].[TenancyContactDetails](
	[TenancyContactId] [int] IDENTITY(1,1) NOT NULL,
	[TenantAddressPostal] [nvarchar](200) NULL,
	[TenantAddressService] [nvarchar](200) NULL,
	[TenantCustomExtra01] [nvarchar](50) NULL,
	[TenantCustomExtra02] [nvarchar](50) NULL,
	[TenantCustomExtra03] [nvarchar](50) NULL,
	[TenantCustomExtra04] [nvarchar](50) NULL,
	[TenantCustomExtra05] [nvarchar](50) NULL,
	[TenantCustomExtra06] [nvarchar](50) NULL,
	[TenantCustomExtra07] [nvarchar](50) NULL,
	[TenantCustomExtra08] [nvarchar](50) NULL,
	[TenantCustomExtra09] [nvarchar](50) NULL,
	[TenantCustomExtra10] [nvarchar](50) NULL,
	[TenantCustomExtra11] [nvarchar](50) NULL,
	[TenantCustomExtra12] [nvarchar](50) NULL,
	[TenantCustomExtra13] [nvarchar](50) NULL,
	[TenantCustomExtra14] [nvarchar](50) NULL,
	[TenantCustomExtra15] [nvarchar](50) NULL,
	[TenantDear] [nvarchar] (50) NULL,
	[TenantEmail] [nvarchar] (50) NULL,
	[TenantFax] [nvarchar] (50) NULL,
	[TenantFirstName] [nvarchar] (50) NULL,
	[TenantFullName] [nvarchar](50) NULL,
	[TenantHead] [bit] NULL,
	[TenantInitials] [nvarchar](50) NULL,
	[TenantLastName] [nvarchar](50) NULL,
	[TenantPaymentMethod] [nvarchar](50) NULL,
	[TenantPhoneHome] [nvarchar](50) NULL,
	[TenantPhoneMobile] [nvarchar](50) NULL,
	[TenantPhoneWork] [nvarchar](50) NULL,
 CONSTRAINT [PK_TenancyContactDetails] PRIMARY KEY CLUSTERED 
(
	[TenancyContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


