USE [getPalace]
GO
/****** Object:  StoredProcedure [dbo].[AddTenancy]    Script Date: 25/10/2019 10:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AddTenancy] 

	@PropertyCode nvarchar(50),
	@TenancyArchived bit,
	@TenancyBankCode nvarchar(50),
	@TenancyBondNumber nvarchar(50),
	@TenancyBondRequired decimal(10, 0),
	@TenancyChangeCode int,
	@TenancyCode nvarchar(50),
	@TenancyContactAddress nvarchar(50),
	@TenancyContactEmail nvarchar(500),
	@TenancyContactFax nvarchar(50),
	@TenancyContactPhone1 nvarchar(50),
	@TenancyContactPhone2 nvarchar(50),
	@TenancyCurrentRent decimal(10, 2),
	@TenancyDateEnded nvarchar (50),
	@TenancyDateStarted nvarchar (50),
	@TenancyDebtor bit,
	@TenancyLastPaymentDate nvarchar (50),
	@TenancyLeaseDateEnded nvarchar (50),
	@TenancyLettingFeeRequired decimal(10, 2),
	@TenancyName nvarchar(50),
	@TenancyNote nvarchar(500),
	@TenancyOtherOwing decimal(10, 2),
	@TenancyOutgoings decimal(10, 2),
	@TenancyRentOwing decimal(18, 0),
	@TenancyRentPaidToDate nvarchar(50),
	@TenancyRentPaidToLastMonth nvarchar(50),
	@TenancyRentPartPayment decimal(18, 0),
	@TenancyRentalPeriod nvarchar(50),
	@TenancySortCode nvarchar(50) 
AS

BEGIN

	/*DECLARE @enddate NVARCHAR(50) = @TenancyDateEnded
	DECLARE @startdate NVARCHAR(50) = @TenancyDateStarted
	DECLARE @lastpaydate NVARCHAR(50) = @TenancyLastPaymentDate
	DECLARE @leasedateend NVARCHAR(50) = @TenancyLeaseDateEnded
	DECLARE @rentpaidend NVARCHAR(50) = @TenancyRentPaidToDate
	DECLARE @rentlastmonth NVARCHAR(50) = @TenancyRentPaidToLastMonth*/

	DECLARE @tempenddate datetime
	DECLARE @tempstartdate datetime
	DECLARE @templastpaydate datetime
	DECLARE @templeasedateend datetime
	DECLARE @temprentpaidend datetime
	DECLARE @temprentlastmonth datetime

	SET @tempenddate = (SELECT [dbo].[ConvertDate](@TenancyDateEnded, 126))
	SET @tempstartdate = (SELECT [dbo].[ConvertDate](@TenancyDateStarted, 126))
	SET @templastpaydate = (SELECT [dbo].[ConvertDate](@TenancyLastPaymentDate, 126))
	SET @templeasedateend = (SELECT [dbo].[ConvertDate](@TenancyLeaseDateEnded, 126))
	SET @temprentpaidend = (SELECT [dbo].[ConvertDate](@TenancyRentPaidToDate, 126))
	SET @temprentlastmonth = (SELECT [dbo].[ConvertDate](@TenancyRentPaidToLastMonth, 126))

	Insert into [dbo].[TenancyDetails]
   (
		PropertyCode,
		TenancyArchived,
		TenancyBankCode,
		TenancyBondNumber,
		TenancyBondRequired,
		TenancyChangeCode,
		TenancyCode,
		TenancyContactAddress,
		TenancyContactEmail,
		TenancyContactFax,
		TenancyContactPhone1,
		TenancyContactPhone2,
		TenancyCurrentRent,
		TenancyDateEnded,
		TenancyDateStarted,
		TenancyDebtor,
		TenancyLastPaymentDate,
		TenancyLeaseDateEnded,
		TenancyLettingFeeRequired,
		TenancyName,
		TenancyNote,
		TenancyOtherOwing,
		TenancyOutgoings,
		TenancyRentOwing,
		TenancyRentPaidToDate,
		TenancyRentPaidToLastMonth,
		TenancyRentPartPayment,
		TenancyRentalPeriod,
		TenancySortCode
   )
   VALUES
   (
		@PropertyCode,
		@TenancyArchived,
		@TenancyBankCode,
		@TenancyBondNumber,
		@TenancyBondRequired,
		@TenancyChangeCode,
		@TenancyCode,
		@TenancyContactAddress,
		@TenancyContactEmail,
		@TenancyContactFax,
		@TenancyContactPhone1,
		@TenancyContactPhone2,
		@TenancyCurrentRent,
		@tempenddate,
		@tempstartdate,
		@TenancyDebtor,
		@templastpaydate,
		@templeasedateend,
		@TenancyLettingFeeRequired,
		@TenancyName,
		@TenancyNote,
		@TenancyOtherOwing,
		@TenancyOutgoings,
		@TenancyRentOwing,
		@temprentpaidend,
		@temprentlastmonth,
		@TenancyRentPartPayment,
		@TenancyRentalPeriod,
		@TenancySortCode
   )  
END