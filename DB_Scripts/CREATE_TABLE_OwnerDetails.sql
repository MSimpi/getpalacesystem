USE [getPalace]
GO

CREATE TABLE [dbo].[OwnerDetails](
	[OwnerId] [int] IDENTITY(1,1) NOT NULL,
	[OwnerAddress] [nvarchar](200) NULL,
	[OwnerArchived] [bit] NULL,
	[OwnerChangeCode] [int] NULL,
	[OwnerCode] [nvarchar](50) NULL,
	[OwnerCountryCode] [nvarchar](50) NULL,
	[OwnerCredits] [decimal](10, 2) NULL,
	[OwnerCurrentBalance] [decimal](10, 2) NULL,
	[OwnerCustom1] [nvarchar](50) NULL,
	[OwnerCustom2] [nvarchar](50) NULL,
	[OwnerCustom3] [nvarchar](50) NULL,
	[OwnerCustom4] [nvarchar](50) NULL,
	[OwnerCustom5] [nvarchar](50) NULL,
	[OwnerCustom6] [nvarchar](50) NULL,
	[OwnerCustom7] [nvarchar](50) NULL,
	[OwnerCustom8] [nvarchar](50) NULL,
	[OwnerDear] [nvarchar](50) NULL,
	[OwnerDebits] [decimal](10, 2) NULL,
	[OwnerEmail1] [nvarchar](50) NULL,
	[OwnerEmail2] [nvarchar](50) NULL,
	[OwnerEndDate] [datetime] NULL,
	[OwnerFax] [nvarchar](50) NULL,
	[OwnerFees] [decimal](10, 2) NULL,
	[OwnerFirstName] [nvarchar](50) NULL,
	[OwnerFullName] [nvarchar](50) NULL,
	[OwnerGST] [decimal](10, 2) NULL,
	[OwnerGSTCollected] [decimal](10, 2) NULL,
	[OwnerGSTExempt] [int] NULL,
	[OwnerHideBankAccount] [bit] NULL,
	[OwnerIncludeAlternateStatement] [bit] NULL,
	[OwnerInitials] [nvarchar](50) NULL,
	[OwnerInsuranceCompany] [nvarchar](50) NULL,
	[OwnerInsuranceExcess] [int] NULL,
	[OwnerLastName] [nvarchar](50) NULL,
	[OwnerLastTransactionDate] [datetime] NULL,
	[OwnerMailMerge] [bit] NULL,
	[OwnerMobile] [nvarchar](50) NULL,
	[OwnerOpeningBalance] [decimal](10, 2) NULL,
	[OwnerOurCode] [nvarchar](50) NULL,
	[OwnerPaymentFrequency] [nvarchar](50) NULL,
	[OwnerPaymentType] [nvarchar](50) NULL,
	[OwnerPayments] [decimal](10, 2) NULL,
	[OwnerPhoneHome] [nvarchar](50) NULL,
	[OwnerPhoneWork] [nvarchar](50) NULL,
	[OwnerPrimaryAgentCode] [nvarchar](50) NULL,
	[OwnerProspect] [bit] NULL,
	[OwnerRentReceived] [decimal](10, 2) NULL,
	[OwnerServiceAddress] [nvarchar](50) NULL,
	[OwnerSortCode] [nvarchar](50) NULL,
	[OwnerStartDate] [datetime] NULL,
	[OwnerStatementMedia] [nvarchar](50) NULL,
	[OwnerStatementNumber] [int] NULL,
	[OwnerStatementType] [nvarchar](50) NULL,
	[OwnerSystemBankCode] [nvarchar](50) NULL,
	[OwnerTitle] [nvarchar](50) NULL,

 CONSTRAINT [PK_OwnerDetails] PRIMARY KEY CLUSTERED 
(
	[OwnerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


