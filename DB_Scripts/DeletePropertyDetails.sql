USE [getPalace]
GO
/****** Object:  StoredProcedure [dbo].[DeletePropertyDetails]    Script Date: 8/09/2019 7:50:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[DeletePropertyDetails]
      
AS

BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
       DELETE from PropertyDetails
	   DBCC CHECKIDENT ('PropertyDetails', RESEED , 0);

END
