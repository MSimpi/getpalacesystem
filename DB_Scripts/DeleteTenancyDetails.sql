USE [getPalace]
GO

CREATE PROCEDURE [dbo].[DeleteTenancyDetails]
      
AS

BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
       DELETE from TenancyDetails
	   DBCC CHECKIDENT ('TenancyDetails', RESEED , 0);

END
