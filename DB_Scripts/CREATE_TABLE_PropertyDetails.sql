USE [getPalace]
GO

/****** Object:  Table [dbo].[PropertyDetails]    Script Date: 7/09/2019 10:33:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PropertyDetails](
	[PropertId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyAddress1] [nvarchar](50) NULL,
	[PropertyAddress2] [nvarchar](50) NULL,
	[PropertyAddress3] [nvarchar](50) NULL,
	[PropertyAddress4] [nvarchar](50) NULL,
	[PropertyAdvert] [nvarchar](500) NULL,
	[PropertyAgentEmail1] [nvarchar](50) NULL,
	[PropertyAgentEmail2] [nvarchar](50) NULL,
	[PropertyAgentFullName] [nvarchar](50) NULL,
	[PropertyArchived] [bit] NULL,
	[PropertyChangeCode] [int] NULL,
	[PropertyCode] [nvarchar](50) NULL,
	[PropertyCommissionPercent] [decimal](10, 2) NULL,
	[PropertyCurrentBalance] [decimal](10, 2) NULL,
	[PropertyDateAvailable] [datetime] NULL,
	[PropertyDisbursementsLimited] [bit] NULL,
	[PropertyDisbursementsMax] [decimal](10, 0) NULL,
	[PropertyGrid] [nvarchar](50) NULL,
	[PropertyKeepBackAccumulationAmount] [decimal](10, 2) NULL,
	[PropertyKeepBackAmount] [decimal](10, 2) NULL,
	[PropertyKeepBackReason] [nvarchar](50) NULL,
	[PropertyKeyAccess] [nvarchar](50) NULL,
	[PropertyKeyNo] [nvarchar](50) NULL,
	[PropertyLAAccountNo] [nvarchar](50) NULL,
	[PropertyLastTransactionDate] [datetime] NULL,
	[PropertyMaintenanceFeePercent] [decimal](18, 0) NULL,
	[PropertyManagementType] [nvarchar](50) NULL,
	[PropertyMarketValue] [decimal](18, 0) NULL,
	[PropertyName] [nvarchar](50) NULL,
	[PropertyOpeningBalance] [decimal](18, 0) NULL,
	[PropertyOurCode] [nvarchar](50) NULL,
	[PropertyOwnerCode] [nvarchar](50) NULL,
	[PropertyPhone] [nvarchar](50) NULL,
	[PropertyPostCode] [nvarchar](50) NULL,
	[PropertyRentAmount] [decimal](10, 2) NULL,
	[PropertyRentalPeriod] [nvarchar](50) NULL,
	[PropertySortCode] [nvarchar](50) NULL,
	[PropertyStartDate] [datetime] NULL,
	[PropertyStatus] [nvarchar](50) NULL,
	[PropertyUnit] [nvarchar](50) NULL,
 CONSTRAINT [PK_PropertyDetails] PRIMARY KEY CLUSTERED 
(
	[PropertId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


