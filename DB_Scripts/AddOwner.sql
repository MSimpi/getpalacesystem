USE [getPalace]
GO
/****** Object:  StoredProcedure [dbo].[AddOwner]    Script Date: 25/10/2019 10:46:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AddOwner] 
	
	@OwnerAddress [nvarchar](200),
	@OwnerArchived [bit],
	@OwnerChangeCode [int],
	@OwnerCode [nvarchar](50),
	@OwnerCountryCode [nvarchar](50),
	@OwnerCredits [decimal](10, 2),
	@OwnerCurrentBalance [decimal](10, 2),
	@OwnerCustom1 [nvarchar](50),
	@OwnerCustom2 [nvarchar](50),
	@OwnerCustom3 [nvarchar](50),
	@OwnerCustom4 [nvarchar](50),
	@OwnerCustom5 [nvarchar](50),
	@OwnerCustom6 [nvarchar](50),
	@OwnerCustom7 [nvarchar](50),
	@OwnerCustom8 [nvarchar](50),
	@OwnerDear [nvarchar](50),
	@OwnerDebits [decimal](10, 2),
	@OwnerEmail1 [nvarchar](50),
	@OwnerEmail2 [nvarchar](50),
	@OwnerEndDate [nvarchar](100),
	@OwnerFax [nvarchar](50),
	@OwnerFees [decimal](10, 2),
	@OwnerFirstName [nvarchar](50),
	@OwnerFullName [nvarchar](50),
	@OwnerGST [decimal](10, 2),
	@OwnerGSTCollected [decimal](10, 2),
	@OwnerGSTExempt [int],
	@OwnerHideBankAccount [bit],
	@OwnerIncludeAlternateStatement [bit],
	@OwnerInitials [nvarchar](50),
	@OwnerInsuranceCompany [nvarchar](50),
	@OwnerInsuranceExcess [int],
	@OwnerLastName [nvarchar](50),
	@OwnerLastTransactionDate [nvarchar](100),
	@OwnerMailMerge [bit],
	@OwnerMobile [nvarchar](50),
	@OwnerOpeningBalance [decimal](10, 2),
	@OwnerOurCode [nvarchar](50),
	@OwnerPaymentFrequency [nvarchar](50),
	@OwnerPaymentType [nvarchar](50),
	@OwnerPayments [decimal](10, 2),
	@OwnerPhoneHome [nvarchar](50),
	@OwnerPhoneWork [nvarchar](50),
	@OwnerPrimaryAgentCode [nvarchar](50),
	@OwnerProspect [bit],
	@OwnerRentReceived [decimal](10, 2),
	@OwnerServiceAddress [nvarchar](50),
	@OwnerSortCode [nvarchar](50),
	@OwnerStartDate [nvarchar](100),
	@OwnerStatementMedia [nvarchar](50),
	@OwnerStatementNumber [int],
	@OwnerStatementType [nvarchar](50),
	@OwnerSystemBankCode [nvarchar](50),
	@OwnerTitle [nvarchar](50)
AS

BEGIN
	
	/*DECLARE @enddate NVARCHAR(50)
	DECLARE @startdate NVARCHAR(50)
	DECLARE @lasttrandate NVARCHAR(100)*/

	DECLARE @tempend datetime
	DECLARE @tempstart datetime
	DECLARE @templasttran datetime

	SET @tempend = (SELECT [dbo].[ConvertDate](@OwnerEndDate, 103))
	SET @tempstart = (SELECT [dbo].[ConvertDate](@OwnerStartDate, 103))
	SET @templasttran = (SELECT [dbo].[ConvertDate](@OwnerLastTransactionDate, 103))
	 
	BEGIN

	Insert into [dbo].[OwnerDetails]
   (
		OwnerAddress,
		OwnerArchived,
		OwnerChangeCode,
		OwnerCode,
		OwnerCountryCode,
		OwnerCredits,
		OwnerCurrentBalance,
		OwnerCustom1,
		OwnerCustom2,
		OwnerCustom3,
		OwnerCustom4,
		OwnerCustom5,
		OwnerCustom6,
		OwnerCustom7,
		OwnerCustom8,
		OwnerDear,
		OwnerDebits,
		OwnerEmail1,
		OwnerEmail2,
		OwnerEndDate,
		OwnerFax,
		OwnerFees,
		OwnerFirstName,
		OwnerFullName,
		OwnerGST,
		OwnerGSTCollected,
		OwnerGSTExempt,
		OwnerHideBankAccount,
		OwnerIncludeAlternateStatement,
		OwnerInitials,
		OwnerInsuranceCompany,
		OwnerInsuranceExcess,
		OwnerLastName,
		OwnerLastTransactionDate,
		OwnerMailMerge,
		OwnerMobile,
		OwnerOpeningBalance,
		OwnerOurCode,
		OwnerPaymentFrequency,
		OwnerPaymentType,
		OwnerPayments,
		OwnerPhoneHome,
		OwnerPhoneWork,
		OwnerPrimaryAgentCode,
		OwnerProspect,
		OwnerRentReceived,
		OwnerServiceAddress,
		OwnerSortCode,
		OwnerStartDate,
		OwnerStatementMedia,
		OwnerStatementNumber,
		OwnerStatementType,
		OwnerSystemBankCode,
		OwnerTitle
   )
   VALUES
   (
		@OwnerAddress,
		@OwnerArchived,
		@OwnerChangeCode,
		@OwnerCode,
		@OwnerCountryCode,
		@OwnerCredits,
		@OwnerCurrentBalance,
		@OwnerCustom1,
		@OwnerCustom2,
		@OwnerCustom3,
		@OwnerCustom4,
		@OwnerCustom5,
		@OwnerCustom6,
		@OwnerCustom7,
		@OwnerCustom8,
		@OwnerDear,
		@OwnerDebits,
		@OwnerEmail1,
		@OwnerEmail2,
		@tempend,
		@OwnerFax,
		@OwnerFees,
		@OwnerFirstName,
		@OwnerFullName,
		@OwnerGST,
		@OwnerGSTCollected,
		@OwnerGSTExempt,
		@OwnerHideBankAccount,
		@OwnerIncludeAlternateStatement,
		@OwnerInitials,
		@OwnerInsuranceCompany,
		@OwnerInsuranceExcess,
		@OwnerLastName,
		@templasttran,
		@OwnerMailMerge,
		@OwnerMobile,
		@OwnerOpeningBalance,
		@OwnerOurCode,
		@OwnerPaymentFrequency,
		@OwnerPaymentType,
		@OwnerPayments,
		@OwnerPhoneHome,
		@OwnerPhoneWork,
		@OwnerPrimaryAgentCode,
		@OwnerProspect,
		@OwnerRentReceived,
		@OwnerServiceAddress,
		@OwnerSortCode,
		@tempstart,
		@OwnerStatementMedia,
		@OwnerStatementNumber,
		@OwnerStatementType,
		@OwnerSystemBankCode,
		@OwnerTitle
   )  
   END
END