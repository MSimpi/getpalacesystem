USE [getPalace]
GO

CREATE TABLE [dbo].[TenancyDetails](
	[TenancyId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyCode] [nvarchar](50) NULL,
	[TenancyArchived] [bit] NULL,
	[TenancyBankCode] [nvarchar](50) NULL,
	[TenancyBondNumber] [nvarchar](50) NULL,
	[TenancyBondRequired] [decimal](10, 0) NULL,
	[TenancyChangeCode] [int] NULL,
	[TenancyCode] [nvarchar](50) NULL,
	[TenancyContactAddress] [nvarchar](50) NULL,
	[TenancyContactEmail] [nvarchar](500) NULL,
	[TenancyContactFax] [nvarchar](50) NULL,
	[TenancyContactPhone1] [nvarchar](50) NULL,
	[TenancyContactPhone2] [nvarchar](50) NULL,
	[TenancyCurrentRent] [decimal](10, 2) NULL,
	[TenancyDateEnded] [datetime] NULL,
	[TenancyDateStarted] [datetime] NULL,
	[TenancyDebtor] [bit] NULL,
	[TenancyLastPaymentDate] [datetime] NULL,
	[TenancyLeaseDateEnded] [datetime] NULL,
	[TenancyLettingFeeRequired] [decimal](10, 2) NULL,
	[TenancyName] [nvarchar](50) NULL,
	[TenancyNote] [nvarchar](500) NULL,
	[TenancyOtherOwing] [decimal](10, 2) NULL,
	[TenancyOutgoings] [decimal](10, 2) NULL,
	[TenancyRentOwing] [decimal](18, 0) NULL,
	[TenancyRentPaidToDate] [datetime] NULL,
	[TenancyRentPaidToLastMonth] [datetime] NULL,
	[TenancyRentPartPayment] [decimal](18, 0) NULL,
	[TenancyRentalPeriod] [nvarchar](50) NULL,
	[TenancySortCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_TenancyDetails] PRIMARY KEY CLUSTERED 
(
	[TenancyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


