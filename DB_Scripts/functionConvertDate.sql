USE [getPalace]
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertDate]    Script Date: 25/10/2019 10:32:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[ConvertDate](@d NVARCHAR(100), @code int)
RETURNS DATETIME
AS
BEGIN
	declare @Return varchar(100)
	if (@d is null or @d = '')
		BEGIN
			SET @Return = null
		END
	ELSE IF @d is not null
		SET @Return = (SELECT CONVERT(datetime, @d, @code));
		Return @Return
	END
