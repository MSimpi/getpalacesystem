USE [getPalace]
GO

CREATE PROCEDURE [dbo].[DeleteTenancyContactDetails]
      
AS

BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
       DELETE from TenancyContactDetails
	   DBCC CHECKIDENT ('TenancyContactDetails', RESEED , 0);

END
