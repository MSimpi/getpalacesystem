USE [getPalace]
GO
/****** Object:  StoredProcedure [dbo].[AddProperty]    Script Date: 25/10/2019 10:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AddProperty] 
	
	@PropertyAddress1 nvarchar (50), 
	@PropertyAddress2 nvarchar (50),
	@PropertyAddress3 nvarchar (50),
	@PropertyAddress4 nvarchar (50),
	@PropertyAdvert nvarchar (500),
	@PropertyAgentEmail1 nvarchar (50),
	@PropertyAgentEmail2 nvarchar (50),
	@PropertyAgentFullName nvarchar (50),
	@PropertyArchived bit,
	@PropertyChangeCode int,
	@PropertyCode nvarchar (50),
	@PropertyCommissionPercent decimal (10,2),
	@PropertyCurrentBalance decimal (10,2),
	@PropertyDateAvailable nvarchar(50),
	@PropertyDisbursementsLimited bit,
	@PropertyDisbursementsMax decimal (10),
	@PropertyGrid nvarchar (50),
	@PropertyKeepBackAccumulationAmount decimal (10,2),
	@PropertyKeepBackAmount decimal (10,2),
	@PropertyKeepBackReason nvarchar (50),
	@PropertyKeyAccess nvarchar (50),
	@PropertyKeyNo nvarchar (50),
	@PropertyLAAccountNo nvarchar (50),
	@PropertyLastTransactionDate nvarchar(50),
	@PropertyMaintenanceFeePercent decimal (10,2),
	@PropertyManagementType nvarchar (50),
	@PropertyMarketValue decimal (10,2),
	@PropertyName nvarchar (50),
	@PropertyOpeningBalance decimal (18,2),
	@PropertyOurCode nvarchar (50),
	@PropertyOwnerCode nvarchar (50),
	@PropertyPhone nvarchar (50),
	@PropertyPostCode nvarchar (50),
	@PropertyRentalPeriod nvarchar(50),
	@PropertyRentAmount decimal (10,2),
	@PropertySortCode nvarchar (50),
	@PropertyStartDate nvarchar (50),
	@PropertyStatus nvarchar (50),
	@PropertyUnit nvarchar (50)
AS

BEGIN

	DECLARE @tempdateavailable datetime
	DECLARE @templasttran datetime
	DECLARE @tempstart datetime

	SET @tempdateavailable = (SELECT [dbo].[ConvertDate](@PropertyDateAvailable, 103))
	SET @templasttran = (SELECT [dbo].[ConvertDate](@PropertyLastTransactionDate, 103))
	SET @tempstart = (SELECT [dbo].[ConvertDate](@PropertyStartDate, 103))

	/*DECLARE @dateavailable NVARCHAR(50) = @PropertyDateAvailable
	DECLARE @lasttrandate NVARCHAR(50) = @PropertyLastTransactionDate
	DECLARE @startdate NVARCHAR(50) = @PropertyStartDate*/

	Insert into [dbo].[PropertyDetails]
   (
		PropertyAddress1, 
		PropertyAddress2,
		PropertyAddress3,
		PropertyAddress4,
		PropertyAdvert,
		PropertyAgentEmail1,
		PropertyAgentEmail2,
		PropertyAgentFullName,
		PropertyArchived,
		PropertyChangeCode,
		PropertyCode,
		PropertyCommissionPercent,
		PropertyCurrentBalance,
		PropertyDateAvailable,
		PropertyDisbursementsLimited,
		PropertyDisbursementsMax,
		PropertyGrid,
		PropertyKeepBackAccumulationAmount,
		PropertyKeepBackAmount,
		PropertyKeepBackReason,
		PropertyKeyAccess,
		PropertyKeyNo,
		PropertyLAAccountNo,
		PropertyLastTransactionDate,
		PropertyMaintenanceFeePercent,
		PropertyManagementType,
		PropertyMarketValue,
		PropertyName,
		PropertyOpeningBalance,
		PropertyOurCode,
		PropertyOwnerCode,
		PropertyPhone,
		PropertyPostCode,
		PropertyRentalPeriod,
		PropertyRentAmount,
		PropertySortCode,
		PropertyStartDate,
		PropertyStatus,
		PropertyUnit
   )
   VALUES
   (
		@PropertyAddress1, 
		@PropertyAddress2,
		@PropertyAddress3,
		@PropertyAddress4,
		@PropertyAdvert,
		@PropertyAgentEmail1,
		@PropertyAgentEmail2,
		@PropertyAgentFullName,
		@PropertyArchived,
		@PropertyChangeCode,
		@PropertyCode,
		@PropertyCommissionPercent,
		@PropertyCurrentBalance,
		@tempdateavailable,
		@PropertyDisbursementsLimited,
		@PropertyDisbursementsMax,
		@PropertyGrid,
		@PropertyKeepBackAccumulationAmount,
		@PropertyKeepBackAmount,
		@PropertyKeepBackReason,
		@PropertyKeyAccess,
		@PropertyKeyNo,
		@PropertyLAAccountNo,
		@templasttran,
		@PropertyMaintenanceFeePercent,
		@PropertyManagementType,
		@PropertyMarketValue,
		@PropertyName,
		@PropertyOpeningBalance,
		@PropertyOurCode,
		@PropertyOwnerCode,
		@PropertyPhone,
		@PropertyPostCode,
		@PropertyRentalPeriod,
		@PropertyRentAmount,
		@PropertySortCode,
		@tempstart,
		@PropertyStatus,
		@PropertyUnit
   )  
END