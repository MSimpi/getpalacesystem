﻿using System.Data.SqlClient;
using System.Configuration;

namespace getPalaceConsoleApp
{
    public class Connection
    {
        public SqlConnection con;
        public void connection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["getpalaceConnection"].ToString();
            con = new SqlConnection(connectionString);
        }
    }
}
