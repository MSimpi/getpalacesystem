﻿using System.Data;
using System.Data.SqlClient;

namespace getPalaceConsoleApp
{
    public class DataAccessLayer : Connection
    {
        public bool CreateProperties(PropertyDetails property)
        {
            connection();

            SqlCommand cmd = new SqlCommand("AddProperty", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@PropertyAddress1", SqlDbType.NVarChar, 50).Value = property.PropertyAddress1;
            cmd.Parameters.Add("@PropertyAddress2", SqlDbType.NVarChar, 50).Value = property.PropertyAddress2;
            cmd.Parameters.Add("@PropertyAddress3", SqlDbType.NVarChar, 50).Value = property.PropertyAddress3;
            cmd.Parameters.Add("@PropertyAddress4", SqlDbType.NVarChar, 50).Value = property.PropertyAddress4;
            cmd.Parameters.Add("@PropertyAdvert", SqlDbType.NVarChar, 500).Value = property.PropertyAdvert;
            cmd.Parameters.Add("@PropertyAgentEmail1", SqlDbType.NVarChar, 50).Value = property.PropertyAgentEmail1;
            cmd.Parameters.Add("@PropertyAgentEmail2", SqlDbType.NVarChar, 50).Value = property.PropertyAgentEmail2;
            cmd.Parameters.Add("@PropertyAgentFullName", SqlDbType.NVarChar, 50).Value = property.PropertyAgentFullName;
            cmd.Parameters.Add("@PropertyArchived", SqlDbType.Bit).Value = property.PropertyArchived;
            cmd.Parameters.Add("@PropertyChangeCode", SqlDbType.Int).Value = property.PropertyChangeCode;
            cmd.Parameters.Add("@PropertyCode", SqlDbType.NVarChar, 50).Value = property.PropertyCode;
            cmd.Parameters.Add("@PropertyCommissionPercent", SqlDbType.Decimal).Value = property.PropertyCommissionPercent;
            cmd.Parameters.Add("@PropertyCurrentBalance", SqlDbType.Decimal).Value = property.PropertyCurrentBalance;
            cmd.Parameters.Add("@PropertyDateAvailable", SqlDbType.NVarChar, 50).Value = property.PropertyDateAvailable;
            cmd.Parameters.Add("@PropertyDisbursementsLimited", SqlDbType.Bit).Value = property.PropertyDisbursementsLimited;
            cmd.Parameters.Add("@PropertyDisbursementsMax", SqlDbType.Decimal).Value = property.PropertyDisbursementsMax;
            cmd.Parameters.Add("@PropertyGrid", SqlDbType.NVarChar, 50).Value = property.PropertyGrid;
            cmd.Parameters.Add("@PropertyKeepBackAccumulationAmount", SqlDbType.Decimal).Value = property.PropertyKeepBackAccumulationAmount;
            cmd.Parameters.Add("@PropertyKeepBackAmount", SqlDbType.Decimal).Value = property.PropertyKeepBackAmount;
            cmd.Parameters.Add("@PropertyKeepBackReason", SqlDbType.NVarChar, 50).Value = property.PropertyKeepBackReason;
            cmd.Parameters.Add("@PropertyKeyAccess", SqlDbType.NVarChar, 50).Value = property.PropertyKeyAccess;
            cmd.Parameters.Add("@PropertyKeyNo", SqlDbType.NVarChar, 50).Value = property.PropertyKeyNo;
            cmd.Parameters.Add("@PropertyLAAccountNo", SqlDbType.NVarChar, 50).Value = property.PropertyLAAccountNo;
            cmd.Parameters.Add("@PropertyLastTransactionDate", SqlDbType.NVarChar, 50).Value = property.PropertyLastTransactionDate;
            cmd.Parameters.Add("@PropertyMaintenanceFeePercent", SqlDbType.Decimal).Value = property.PropertyMaintenanceFeePercent;
            cmd.Parameters.Add("@PropertyManagementType", SqlDbType.NVarChar, 50).Value = property.PropertyManagementType;
            cmd.Parameters.Add("@PropertyMarketValue", SqlDbType.Decimal).Value = property.PropertyMarketValue;
            cmd.Parameters.Add("@PropertyName", SqlDbType.NVarChar, 50).Value = property.PropertyName;
            cmd.Parameters.Add("@PropertyOpeningBalance", SqlDbType.Decimal).Value = property.PropertyOpeningBalance;
            cmd.Parameters.Add("@PropertyOurCode", SqlDbType.NVarChar, 50).Value = property.PropertyOurCode;
            cmd.Parameters.Add("@PropertyOwnerCode", SqlDbType.NVarChar, 50).Value = property.PropertyOwnerCode;
            cmd.Parameters.Add("@PropertyPhone", SqlDbType.NVarChar, 50).Value = property.PropertyPhone;
            cmd.Parameters.Add("@PropertyPostCode", SqlDbType.NVarChar, 50).Value = property.PropertyPostCode;
            cmd.Parameters.Add("@PropertyRentalPeriod", SqlDbType.NVarChar, 50).Value = property.PropertyRentalPeriod;
            cmd.Parameters.Add("@PropertyRentAmount", SqlDbType.Decimal).Value = property.PropertyRentAmount;
            cmd.Parameters.Add("@PropertySortCode", SqlDbType.NVarChar, 50).Value = property.PropertySortCode;
            cmd.Parameters.Add("@PropertyStartDate", SqlDbType.NVarChar, 50).Value = property.PropertyStartDate;
            cmd.Parameters.Add("@PropertyStatus", SqlDbType.NVarChar, 50).Value = property.PropertyStatus;
            cmd.Parameters.Add("@PropertyUnit", SqlDbType.NVarChar, 50).Value = property.PropertyUnit;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return true;
        }
        public void DeleteProperties()
        {
            connection();

            SqlCommand cmdDelete = new SqlCommand("DeletePropertyDetails", con);
            cmdDelete.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmdDelete.ExecuteNonQuery();
            con.Close();
        }
        public bool CreateTenancy(TenancyDetails tenancy)
        {
            connection();

            SqlCommand cmd = new SqlCommand("AddTenancy", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@PropertyCode", SqlDbType.NVarChar, 50).Value = tenancy.PropertyCode;
            cmd.Parameters.Add("@TenancyArchived", SqlDbType.Bit).Value = tenancy.TenancyArchived;
            cmd.Parameters.Add("@TenancyBankCode", SqlDbType.NVarChar, 50).Value = tenancy.TenancyBankCode;
            cmd.Parameters.Add("@TenancyBondNumber", SqlDbType.NVarChar, 50).Value = tenancy.TenancyBondNumber;
            cmd.Parameters.Add("@TenancyBondRequired", SqlDbType.Decimal).Value = tenancy.TenancyBondRequired;
            cmd.Parameters.Add("@TenancyChangeCode", SqlDbType.Int).Value = tenancy.TenancyChangeCode;
            cmd.Parameters.Add("@TenancyCode", SqlDbType.NVarChar, 50).Value = tenancy.TenancyCode;
            cmd.Parameters.Add("@TenancyContactAddress", SqlDbType.NVarChar, 50).Value = tenancy.TenancyContactAddress;
            cmd.Parameters.Add("@TenancyContactEmail", SqlDbType.NVarChar, 50).Value = tenancy.TenancyContactEmail;
            cmd.Parameters.Add("@TenancyContactFax", SqlDbType.NVarChar, 50).Value = tenancy.TenancyContactFax;
            cmd.Parameters.Add("@TenancyContactPhone1", SqlDbType.NVarChar, 50).Value = tenancy.TenancyContactPhone1;
            cmd.Parameters.Add("@TenancyContactPhone2", SqlDbType.NVarChar, 50).Value = tenancy.TenancyContactPhone2;
            cmd.Parameters.Add("@TenancyCurrentRent", SqlDbType.Decimal).Value = tenancy.TenancyCurrentRent;
            cmd.Parameters.Add("@TenancyDateEnded", SqlDbType.NVarChar, 50).Value = tenancy.TenancyDateEnded;
            cmd.Parameters.Add("@TenancyDateStarted", SqlDbType.NVarChar, 50).Value = tenancy.TenancyDateStarted;
            cmd.Parameters.Add("@TenancyDebtor", SqlDbType.Bit).Value = tenancy.TenancyDebtor;
            cmd.Parameters.Add("@TenancyLastPaymentDate", SqlDbType.NVarChar, 50).Value = tenancy.TenancyLastPaymentDate;
            cmd.Parameters.Add("@TenancyLeaseDateEnded", SqlDbType.NVarChar, 50).Value = tenancy.TenancyLeaseDateEnded;
            cmd.Parameters.Add("@TenancyLettingFeeRequired", SqlDbType.Decimal).Value = tenancy.TenancyLettingFeeRequired;
            cmd.Parameters.Add("@TenancyName", SqlDbType.NVarChar, 50).Value = tenancy.TenancyName;
            cmd.Parameters.Add("@TenancyNote", SqlDbType.NVarChar, 500).Value = tenancy.TenancyNote;
            cmd.Parameters.Add("@TenancyOtherOwing", SqlDbType.Decimal).Value = tenancy.TenancyOtherOwing;
            cmd.Parameters.Add("@TenancyOutgoings", SqlDbType.Decimal).Value = tenancy.TenancyOutgoings;
            cmd.Parameters.Add("@TenancyRentOwing", SqlDbType.Decimal).Value = tenancy.TenancyRentOwing;
            cmd.Parameters.Add("@TenancyRentPaidToDate", SqlDbType.NVarChar, 50).Value = tenancy.TenancyRentPaidToDate;
            cmd.Parameters.Add("@TenancyRentPaidToLastMonth", SqlDbType.NVarChar, 50).Value = tenancy.TenancyRentPaidToLastMonth;
            cmd.Parameters.Add("@TenancyRentPartPayment", SqlDbType.Decimal).Value = tenancy.TenancyRentPartPayment;
            cmd.Parameters.Add("@TenancyRentalPeriod", SqlDbType.NVarChar, 50).Value = tenancy.TenancyRentalPeriod;
            cmd.Parameters.Add("@TenancySortCode", SqlDbType.NVarChar, 50).Value = tenancy.TenancySortCode;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return true;
        }
        public void DeleteTenancy()
        {
            connection();

            SqlCommand cmdDelete = new SqlCommand("DeleteTenancyDetails", con);
            cmdDelete.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmdDelete.ExecuteNonQuery();
            con.Close();
        }
        public bool CreateTenancyContact(TenancyContactDetails tenancyContactDetails)
        {
            connection();

            SqlCommand cmd = new SqlCommand("AddTenancyContact", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@TenantAddressPostal", SqlDbType.NVarChar, 200).Value = tenancyContactDetails.TenantAddressPostal;
            cmd.Parameters.Add("@TenantAddressService", SqlDbType.NVarChar, 200).Value = tenancyContactDetails.TenantAddressService;
            cmd.Parameters.Add("@TenantCustomExtra01", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra01;
            cmd.Parameters.Add("@TenantCustomExtra02", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra02;
            cmd.Parameters.Add("@TenantCustomExtra03", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra03;
            cmd.Parameters.Add("@TenantCustomExtra04", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra04;
            cmd.Parameters.Add("@TenantCustomExtra05", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra05;
            cmd.Parameters.Add("@TenantCustomExtra06", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra06;
            cmd.Parameters.Add("@TenantCustomExtra07", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra07;
            cmd.Parameters.Add("@TenantCustomExtra08", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra08;
            cmd.Parameters.Add("@TenantCustomExtra09", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra09;
            cmd.Parameters.Add("@TenantCustomExtra10", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra10;
            cmd.Parameters.Add("@TenantCustomExtra11", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra11;
            cmd.Parameters.Add("@TenantCustomExtra12", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra12;
            cmd.Parameters.Add("@TenantCustomExtra13", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra13;
            cmd.Parameters.Add("@TenantCustomExtra14", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra14;
            cmd.Parameters.Add("@TenantCustomExtra15", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantCustomExtra15;
            cmd.Parameters.Add("@TenantDear", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantDear;
            cmd.Parameters.Add("@TenantEmail", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantEmail;
            cmd.Parameters.Add("@TenantFax", SqlDbType.NVarChar, 500).Value = tenancyContactDetails.TenantFax;
            cmd.Parameters.Add("@TenantFirstName", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantFirstName;
            cmd.Parameters.Add("@TenantFullName", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantFullName;
            cmd.Parameters.Add("@TenantHead", SqlDbType.Bit).Value = tenancyContactDetails.TenantHead;
            cmd.Parameters.Add("@TenantInitials", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantInitials;
            cmd.Parameters.Add("@TenantLastName", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantLastName;
            cmd.Parameters.Add("@TenantPaymentMethod", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantPaymentMethod;
            cmd.Parameters.Add("@TenantPhoneHome", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantPhoneHome;
            cmd.Parameters.Add("@TenantPhoneMobile", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantPhoneMobile;
            cmd.Parameters.Add("@TenantPhoneWork", SqlDbType.NVarChar, 50).Value = tenancyContactDetails.TenantPhoneWork;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return true;
        }
        public void DeleteTenancyContact()
        {
            connection();

            SqlCommand cmdDelete = new SqlCommand("DeleteTenancyContactDetails", con);
            cmdDelete.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmdDelete.ExecuteNonQuery();
            con.Close();
        }
        public bool CreateOwner(OwnerDetails owner)
        {
            connection();

            SqlCommand cmd = new SqlCommand("AddOwner", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@OwnerAddress", SqlDbType.NVarChar, 50).Value = owner.OwnerAddress;
            cmd.Parameters.Add("@OwnerArchived", SqlDbType.Bit).Value = owner.OwnerArchived;
            cmd.Parameters.Add("@OwnerChangeCode", SqlDbType.Int).Value = owner.OwnerChangeCode;
            cmd.Parameters.Add("@OwnerCode", SqlDbType.NVarChar, 50).Value = owner.OwnerCode;
            cmd.Parameters.Add("@OwnerCountryCode", SqlDbType.NVarChar, 50).Value = owner.OwnerCountryCode;
            cmd.Parameters.Add("@OwnerCredits", SqlDbType.Decimal).Value = owner.OwnerCredits;
            cmd.Parameters.Add("@OwnerCurrentBalance", SqlDbType.Decimal).Value = owner.OwnerCurrentBalance;
            cmd.Parameters.Add("@OwnerCustom1", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom1;
            cmd.Parameters.Add("@OwnerCustom2", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom2;
            cmd.Parameters.Add("@OwnerCustom3", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom3;
            cmd.Parameters.Add("@OwnerCustom4", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom4;
            cmd.Parameters.Add("@OwnerCustom5", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom5;
            cmd.Parameters.Add("@OwnerCustom6", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom6;
            cmd.Parameters.Add("@OwnerCustom7", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom7;
            cmd.Parameters.Add("@OwnerCustom8", SqlDbType.NVarChar, 50).Value = owner.OwnerCustom8;
            cmd.Parameters.Add("@OwnerDear", SqlDbType.NVarChar, 50).Value = owner.OwnerDear;
            cmd.Parameters.Add("@OwnerDebits", SqlDbType.Decimal).Value = owner.OwnerDebits;
            cmd.Parameters.Add("@OwnerEmail1", SqlDbType.NVarChar, 50).Value = owner.OwnerEmail1;
            cmd.Parameters.Add("@OwnerEmail2", SqlDbType.NVarChar, 50).Value = owner.OwnerEmail2;
            cmd.Parameters.Add("@OwnerEndDate", SqlDbType.NVarChar, 100).Value = owner.OwnerEndDate;
            cmd.Parameters.Add("@OwnerFax", SqlDbType.NVarChar, 50).Value = owner.OwnerFax;
            cmd.Parameters.Add("@OwnerFees", SqlDbType.Decimal).Value = owner.OwnerFees;
            cmd.Parameters.Add("@OwnerFirstName", SqlDbType.NVarChar, 50).Value = owner.OwnerFirstName;
            cmd.Parameters.Add("@OwnerFullName", SqlDbType.NVarChar, 50).Value = owner.OwnerFullName;
            cmd.Parameters.Add("@OwnerGST", SqlDbType.Decimal).Value = owner.OwnerGST;
            cmd.Parameters.Add("@OwnerGSTCollected", SqlDbType.Decimal).Value = owner.OwnerGSTCollected;
            cmd.Parameters.Add("@OwnerGSTExempt", SqlDbType.Int).Value = owner.OwnerGSTExempt;
            cmd.Parameters.Add("@OwnerHideBankAccount", SqlDbType.Bit).Value = owner.OwnerHideBankAccount;
            cmd.Parameters.Add("@OwnerIncludeAlternateStatement", SqlDbType.Bit).Value = owner.OwnerIncludeAlternateStatement;
            cmd.Parameters.Add("@OwnerInitials", SqlDbType.NVarChar, 50).Value = owner.OwnerInitials;
            cmd.Parameters.Add("@OwnerInsuranceCompany", SqlDbType.NVarChar, 50).Value = owner.OwnerInsuranceCompany;
            cmd.Parameters.Add("@OwnerInsuranceExcess", SqlDbType.Int).Value = owner.OwnerInsuranceExcess;
            cmd.Parameters.Add("@OwnerLastName", SqlDbType.NVarChar, 50).Value = owner.OwnerLastName;
            cmd.Parameters.Add("@OwnerLastTransactionDate", SqlDbType.NVarChar, 100).Value = owner.OwnerLastTransactionDate;
            cmd.Parameters.Add("@OwnerMailMerge", SqlDbType.Bit).Value = owner.OwnerMailMerge;
            cmd.Parameters.Add("@OwnerMobile", SqlDbType.NVarChar, 50).Value = owner.OwnerMobile;
            cmd.Parameters.Add("@OwnerOpeningBalance", SqlDbType.Decimal).Value = owner.OwnerOpeningBalance;
            cmd.Parameters.Add("@OwnerOurCode", SqlDbType.NVarChar, 50).Value = owner.OwnerOurCode;
            cmd.Parameters.Add("@OwnerPaymentFrequency", SqlDbType.NVarChar, 50).Value = owner.OwnerPaymentFrequency;
            cmd.Parameters.Add("@OwnerPaymentType", SqlDbType.NVarChar, 50).Value = owner.OwnerPaymentType;
            cmd.Parameters.Add("@OwnerPayments", SqlDbType.Decimal).Value = owner.OwnerPayments;
            cmd.Parameters.Add("@OwnerPhoneHome", SqlDbType.NVarChar, 50).Value = owner.OwnerPhoneHome;
            cmd.Parameters.Add("@OwnerPhoneWork", SqlDbType.NVarChar, 50).Value = owner.OwnerPhoneWork;
            cmd.Parameters.Add("@OwnerPrimaryAgentCode", SqlDbType.NVarChar, 50).Value = owner.OwnerPrimaryAgentCode;
            cmd.Parameters.Add("@OwnerProspect", SqlDbType.Bit).Value = owner.OwnerProspect;
            cmd.Parameters.Add("@OwnerRentReceived", SqlDbType.Decimal).Value = owner.OwnerRentReceived;
            cmd.Parameters.Add("@OwnerServiceAddress", SqlDbType.NVarChar, 50).Value = owner.OwnerServiceAddress;
            cmd.Parameters.Add("@OwnerSortCode", SqlDbType.NVarChar, 50).Value = owner.OwnerSortCode;
            cmd.Parameters.Add("@OwnerStartDate", SqlDbType.NVarChar, 100).Value = owner.OwnerStartDate;
            cmd.Parameters.Add("@OwnerStatementMedia", SqlDbType.NVarChar, 50).Value = owner.OwnerStatementMedia;
            cmd.Parameters.Add("@OwnerStatementNumber", SqlDbType.Int).Value = owner.OwnerStatementNumber;
            cmd.Parameters.Add("@OwnerStatementType", SqlDbType.NVarChar, 50).Value = owner.OwnerStatementType;
            cmd.Parameters.Add("@OwnerSystemBankCode", SqlDbType.NVarChar, 50).Value = owner.OwnerSystemBankCode;
            cmd.Parameters.Add("@OwnerTitle", SqlDbType.NVarChar, 50).Value = owner.OwnerTitle;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            return true;
        }
        public void DeleteOwner()
        {
            connection();

            SqlCommand cmdDelete = new SqlCommand("DeleteOwnerDetails", con);
            cmdDelete.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmdDelete.ExecuteNonQuery();
            con.Close();
        }
    }
}
