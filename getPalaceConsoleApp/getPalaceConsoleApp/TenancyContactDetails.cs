﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getPalaceConsoleApp
{
    public class TenancyContactDetails
    {
        public string TenantAddressPostal { get; set; }
        public string TenantAddressService { get; set; }
        public string TenantCustomExtra01 { get; set; }
        public string TenantCustomExtra02 { get; set; }
        public string TenantCustomExtra03 { get; set; }
        public string TenantCustomExtra04 { get; set; }
        public string TenantCustomExtra05 { get; set; }
        public string TenantCustomExtra06 { get; set; }
        public string TenantCustomExtra07 { get; set; }
        public string TenantCustomExtra08 { get; set; }
        public string TenantCustomExtra09 { get; set; }
        public string TenantCustomExtra10 { get; set; }
        public string TenantCustomExtra11 { get; set; }
        public string TenantCustomExtra12 { get; set; }
        public string TenantCustomExtra13 { get; set; }
        public string TenantCustomExtra14 { get; set; }
        public string TenantCustomExtra15 { get; set; }
        public string TenantDear { get; set; }
        public string TenantEmail { get; set; }
        public string TenantFax { get; set; }
        public string TenantFirstName { get; set; }
        public string TenantFullName { get; set; }
        public bool TenantHead { get; set; }
        public string TenantInitials { get; set; }
        public string TenantLastName { get; set; }
        public string TenantPaymentMethod { get; set; }
        public string TenantPhoneHome { get; set; }
        public string TenantPhoneMobile { get; set; }
        public string TenantPhoneWork { get; set; }
    }
}
