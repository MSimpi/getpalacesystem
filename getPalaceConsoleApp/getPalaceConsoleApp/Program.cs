﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace getPalaceConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PropertyAction();
            OwnerAction();
            TenancyContactAction();
            TenancyAction();
        }

        public static void PropertyAction()
        {
            try
            {
                new DataAccessLayer().DeleteProperties();

                WebClient client = new WebClient();
                client.UseDefaultCredentials = true;
                client.Credentials = new NetworkCredential("api-dev@impression.co.nz", "iMpr35510n492395689DeC");

                var result = client.DownloadString("https://api.getpalace.com/Service.svc/RestService/v2ViewAllDetailedProperty/JSON");

                PropertyDetails[] propertyarray = JsonConvert.DeserializeObject<PropertyDetails[]>(result);
                DataAccessLayer dataAccessLayer = new DataAccessLayer();

                foreach (PropertyDetails proparray in propertyarray)
                {
                    dataAccessLayer.CreateProperties(proparray);
                }
                Console.WriteLine("Property Details Successfully Added..");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong.." + ex);
            }
        }
        public static void TenancyAction()
        {
            try
            {
                new DataAccessLayer().DeleteTenancy();

                WebClient client = new WebClient();
                client.UseDefaultCredentials = true;
                client.Credentials = new NetworkCredential("api-dev@impression.co.nz", "iMpr35510n492395689DeC");

                var result = client.DownloadString("https://api.getpalace.com/Service.svc/RestService/v2ViewAllDetailedTenancy/JSON");

                TenancyDetails[] tenancyarray = JsonConvert.DeserializeObject<TenancyDetails[]>(result);
                DataAccessLayer dataAccessLayer = new DataAccessLayer();

                foreach (TenancyDetails tenantarray in tenancyarray)
                {
                    dataAccessLayer.CreateTenancy(tenantarray);
                }
                Console.WriteLine("Tenancy Details Successfully Added..");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong.." + ex);
            }
        }
        public static void TenancyContactAction()
        {
            try
            {
                new DataAccessLayer().DeleteTenancyContact();

                WebClient client = new WebClient();
                client.UseDefaultCredentials = true;
                client.Credentials = new NetworkCredential("api-dev@impression.co.nz", "iMpr35510n492395689DeC");

                var result = client.DownloadString("https://api.getpalace.com/Service.svc/RestService/v2ViewAllDetailedTenancy/JSON");

                TenancyDetails[] tenancycontactarray = JsonConvert.DeserializeObject<TenancyDetails[]>(result);

                DataAccessLayer dataAccessLayer = new DataAccessLayer();

                foreach (TenancyDetails tenantcontactarray in tenancycontactarray)
                {
                    foreach (TenancyContactDetails tc in tenantcontactarray.TenancyTenants)
                    {
                        dataAccessLayer.CreateTenancyContact(tc);
                    }
                }
                Console.WriteLine("Tenancy Contact Details Successfully Added..");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong.." + ex);
            }
        }
        public static void OwnerAction()
        {
            try
            {
                new DataAccessLayer().DeleteOwner();

                WebClient client = new WebClient();
                client.UseDefaultCredentials = true;
                client.Credentials = new NetworkCredential("api-dev@impression.co.nz", "iMpr35510n492395689DeC");

                var result = client.DownloadString("https://api.getpalace.com/Service.svc/RestService/v2ViewAllDetailedOwner/JSON");

                OwnerDetails[] ownerarray = JsonConvert.DeserializeObject<OwnerDetails[]>(result);
                DataAccessLayer dataAccessLayer = new DataAccessLayer();
                
                foreach (OwnerDetails ownarray in ownerarray)
                {
                    dataAccessLayer.CreateOwner(ownarray);
                }
                Console.WriteLine("Owner Details Successfully Added..");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong.." + ex);
            }
        }
    }
}
