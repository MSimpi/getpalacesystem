﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getPalaceConsoleApp
{
    public class OwnerDetails
    {
        public string OwnerAddress { get; set; }
        public bool OwnerArchived { get; set; }
        public int OwnerChangeCode { get; set; }
        public string OwnerCode { get; set; }
        public string OwnerCountryCode { get; set; }
        public decimal OwnerCredits { get; set; }
        public decimal OwnerCurrentBalance { get; set; }
        public string OwnerCustom1 { get; set; }
        public string OwnerCustom2 { get; set; }
        public string OwnerCustom3 { get; set; }
        public string OwnerCustom4 { get; set; }
        public string OwnerCustom5 { get; set; }
        public string OwnerCustom6 { get; set; }
        public string OwnerCustom7 { get; set; }
        public string OwnerCustom8 { get; set; }
        public string OwnerDear { get; set; }
        public decimal OwnerDebits { get; set; }
        public string OwnerEmail1 { get; set; }
        public string OwnerEmail2 { get; set; }
        public string OwnerEndDate { get; set; }
        public string OwnerFax { get; set; }
        public decimal OwnerFees { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerFullName { get; set; }
        public decimal OwnerGST { get; set; }
        public decimal OwnerGSTCollected { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int OwnerGSTExempt { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool OwnerHideBankAccount { get; set; }
        public bool OwnerIncludeAlternateStatement { get; set; }
        public string OwnerInitials { get; set; }
        public string OwnerInsuranceCompany { get; set; }
        public int OwnerInsuranceExcess { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerLastTransactionDate { get; set; }
        public bool OwnerMailMerge { get; set; }
        public string OwnerMobile { get; set; }
        public decimal OwnerOpeningBalance { get; set; }
        public string OwnerOurCode { get; set; }
        public string OwnerPaymentFrequency { get; set; }
        public string OwnerPaymentType { get; set; }
        public decimal OwnerPayments { get; set; }
        public string OwnerPhoneHome { get; set; }
        public string OwnerPhoneWork { get; set; }
        public string OwnerPrimaryAgentCode { get; set; }
        public bool OwnerProspect { get; set; }
        public decimal OwnerRentReceived { get; set; }
        public string OwnerServiceAddress { get; set; }
        public string OwnerSortCode { get; set; }
        public string OwnerStartDate { get; set; }
        public string OwnerStatementMedia { get; set; }
        public int OwnerStatementNumber { get; set; }
        public string OwnerStatementType { get; set; }
        public string OwnerSystemBankCode { get; set; }
        public string OwnerTitle { get; set; }
    }
}
