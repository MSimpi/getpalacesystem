﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getPalaceConsoleApp
{
    public class PropertyDetails
    {
        public string PropertyAddress1 { get; set; }
        public string PropertyAddress2 { get; set; }
        public string PropertyAddress3 { get; set; }
        public string PropertyAddress4 { get; set; }
        public string PropertyAdvert { get; set; }
        public string PropertyAgentEmail1 { get; set; }
        public string PropertyAgentEmail2 { get; set; }
        public string PropertyAgentFullName { get; set; }
        public bool PropertyArchived { get; set; }
        public int PropertyChangeCode { get; set; }
        public string PropertyCode { get; set; }
        public decimal PropertyCommissionPercent { get; set; }
        public decimal PropertyCurrentBalance { get; set; }
        public string PropertyDateAvailable { get; set; }
        public bool PropertyDisbursementsLimited { get; set; }
        public decimal PropertyDisbursementsMax { get; set; }
        public string PropertyGrid { get; set; }
        public decimal PropertyKeepBackAccumulationAmount { get; set; }
        public decimal PropertyKeepBackAmount { get; set; }
        public string PropertyKeepBackReason { get; set; }
        public string PropertyKeyAccess { get; set; }
        public string PropertyKeyNo { get; set; }
        public string PropertyLAAccountNo { get; set; }
        public string PropertyLastTransactionDate { get; set; }
        public decimal PropertyMaintenanceFeePercent { get; set; }
        public string PropertyManagementType { get; set; }
        public decimal PropertyMarketValue { get; set; }
        public string PropertyName { get; set; }
        public decimal PropertyOpeningBalance { get; set; }
        public string PropertyOurCode { get; set; }
        public string PropertyOwnerCode { get; set; }
        public string PropertyPhone { get; set; }
        public string PropertyPostCode { get; set; }
        public decimal PropertyRentAmount { get; set; }
        public string PropertyRentalPeriod { get; set; }
        public string PropertySortCode { get; set; }
        public string PropertyStartDate { get; set; }
        public string PropertyStatus { get; set; }
        public string PropertyUnit { get; set; }
    }
}
