﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getPalaceConsoleApp
{
    public class TenancyDetails
    {
        public string PropertyCode { get; set; }
        public bool TenancyArchived { get; set; }
        public string TenancyBankCode { get; set; }
        public string TenancyBondNumber { get; set; }
        public decimal TenancyBondRequired { get; set; }
        public int TenancyChangeCode { get; set; }
        public string TenancyCode { get; set; }
        public string TenancyContactAddress { get; set; }
        public string TenancyContactEmail { get; set; }
        public string TenancyContactFax { get; set; }
        public string TenancyContactPhone1 { get; set; }
        public string TenancyContactPhone2 { get; set; }
        public decimal TenancyCurrentRent { get; set; }
        public string TenancyDateEnded { get; set; }
        public string TenancyDateStarted { get; set; }
        public bool TenancyDebtor { get; set; }
        public string TenancyLastPaymentDate { get; set; }
        public string TenancyLeaseDateEnded { get; set; }
        public decimal TenancyLettingFeeRequired { get; set; }
        public string TenancyName { get; set; }
        public string TenancyNote { get; set; }
        public decimal TenancyOtherOwing { get; set; }
        public decimal TenancyOutgoings { get; set; }
        public decimal TenancyRentOwing { get; set; }
        public string TenancyRentPaidToDate { get; set; }
        public string TenancyRentPaidToLastMonth { get; set; }
        public decimal TenancyRentPartPayment { get; set; }
        public string TenancyRentalPeriod { get; set; }
        public string TenancySortCode { get; set; }
        public List<TenancyContactDetails> TenancyTenants { get; set; }
    }
}
